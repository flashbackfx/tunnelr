tunnelr:
    a demoscene-like raytraced tunnel demo
        by Juanmi Lopez

  tunnelr is a demoscene-like raytraced tunnel demo program made in C to
test the raylib library and to experiment with the demoscene tunnel effect
(of the raytraced kind). It was made using information from a text written
in 1997 by BlackAxe, from the KoLOr demo group, found in the Fabien Sanglard's
Website article "RayTracing" (https://fabiensanglard.net/RayTracing/index.php).
Fabien's Java source was also used as reference.

  The program is able to load arbitrary textures for the tunnel and lets
the user change tunnel parameters during runtime.


Usage
=====

  Run the program providing the path of an image file as the first argument.
If no image file is specified the program will try to load a default file,
the name of which is defined in the source code (currently, default.png).
  During runtime various parameters can be altered using the following 
controls:

 --Tunnel appearance:

   Q    // Increase the value of the origin vector z component
   A    // Decrease the value of the origin vector z component
   W    // Increase the value of the direction vector z component
   S    // Decrease the value of the direction vector z component

   E    // Increase the radius of the tunnel
   D    // Decrease the radius of the tunnel
   R    // Increase the texture U ratio
   F    // Decrease the texture U ratio
   T    // Increase the texture V ratio
   G    // Decrease the texture V ratio

 --Movement across the tunnel:

   UP   // Advance forwards through the tunnel
  DOWN  // Advance backwards through the tunnel
  LEFT  // Advance laterally through the tunnel / rotate the tunnel
 RIGHT  // Advance laterally through the tunnel / rotate the tunnel
   M    // Stop movement

 --Origin vector position in the (x, y) plane (position in tunnel relative
   to its center):

  KP_8  // Move the origin vector up in the (x, y) plane
  KP_2  // Move the origin vector down in the (x, y) plane
  KP_6  // Move the origin vector to the right in the (x, y) plane
  KP_4  // Move the origin vector to the left in the (x, y) plane
  KP_5  // Reset the origin vector position to the center of the tunnel

 --Direction vectors offset in the (x, y) plane (look around the tunnel):

   Y    // Move offset up in the (x, y) plane
   H    // Move offset down in the (x, y) plane
   I    // Move offset left in the (x, y) plane
   U    // Move offset right in the (x, y) plane
   J    // Reset offset position to the center of the screen

 --Rotation of the direction vectors in the z axis (viewpoint rotation):

  KP_9  // Rotate left
  KP_7  // Rotate right
  KP_1  // Reset rotation to initial value

 --Option toggling:

   0    // Toggles between the "movement free" and "movement friction" modes
   9    // Toggles the "fixed angle camera" on and off
   K    // Toggles between locked and unlocked FPS
   P    // Toggles tunnel parameter information display

 --Interpolation parameters:

   1    // Decrease interpolation step in both axis
   2    // Increase interpolation step in both axis
   3    // Decrease interpolation step in the y axis
   4    // Increase interpolation step in the y axis
   5    // Decrease interpolation step in the x axis
   6    // Increase interpolation step in the x axis
   7    // Set the interpolation step value for the y axis to the one for the x axis
   8    // Set the interpolation step value for the x axis to the one for the y axis


Notes
=====

  The sample code in the BlackAxe text and Fabien Sanglard's implementation
map the texture in a way that gets mirrored halfway across V (laterally).
In tunnelr this has been changed to make the texture width map to the full
circle that defines the tunnel.

  The V ratio governs how the texture is mapped laterally to the surface
of the tunnel. A value equal to the texture width means the texture is
mapped once for the circle that forms the tunnel. The control that alters
the V ratio does it in steps of value equal to the texture width.

  BlackAxe talks about interpolating across the UV set (texture coordinates)
wording it in a way that suggests bilinear interpolation. Fabien Sanglard's
implementation does linear interpolation vertically. tunnelr does bilinear
interpolation using the lerp and blerp functions taken from the Rosetta
Code website "Bilinear Interpolation" entry (https://rosettacode.org/wiki/Bilinear_interpolation).

  Visual glitches will occur when different parameters are set to certain
values. Some of them are: when the origin vector z and the direction vector
z have the same sign, when the V or U ratios are negative, when the origin
vector position in (x, y) is outside of the circumference defined by the
tunnel radius, and many more. These make for interesting effects, and the
user is encouraged to find even more interesting combinations.

  There are two movement modes: "movement free", which holds the last
speed value for each axis, and "movement friction", which slows down
until it stops if no movement key is pressed.

  Movement across the tunnel (both frontal and lateral) is simulated by
shifting its texture (across its height and width respectively). Frontal
movement can also be simulated by altering the origin vector z component
but the glitch point (in which the direction vector and the origin vector
have same sign) could be reached. Lateral movement can be simulated when 
the origin vector is in the center of the tunnel by rotating the direction
vectors, but not in other positions. Another difference is that tunnelr
uses acceleration for the "movement" (texture shifting) simulation, but
not for the alteration of vector properties. The acceleration and maximum
speed depend on the size of the texture.

  When the "fixed angle camera" mode is set, the texture does not shift
laterally when moving. The viewpoint rotates around the center of the
tunnel instead.

  The angle to which the direction vectors are rotated affect the control
of the origin vector in the (x, y) plane. That means that, for example,
if the direction vectors are rotated 180 degrees, moving the origin vector
to the left will be ultimately seen as a movement to the right. Said (x, y)
plane is rotated with the direction vectors.

  The accepted image formats will depend on the flags used to compile the
raylib library.


Additional information
======================

  tunnelr has been developed and tested using the C (99) programming
language on a Debian based GNU/Linux system running on an x64 machine,
using the gcc compiler version 7.3.0, the raylib library version
2.4-dev compiled with the PLATFORM_DESKTOP flag and the OpenGL 3.3 backend,
and the C standard library.